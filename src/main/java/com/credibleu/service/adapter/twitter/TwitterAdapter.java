package com.credibleu.service.adapter.twitter;

import com.credibleu.dao.SentWarningRepository;
import com.credibleu.dao.TargetPersonRepository;
import com.credibleu.domain.Post;
import com.credibleu.domain.SentWarning;
import com.credibleu.domain.SocialMedia;
import com.credibleu.domain.TargetPerson;
import com.credibleu.domain.event.FakePostDetectedEvent;
import com.credibleu.domain.event.NewTargetPersonAddedEvent;
import com.credibleu.utils.Index;
import com.google.common.collect.Lists;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.event.EventListener;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.PostConstruct;
import java.util.List;
import java.util.Queue;

/**
 * Adapter working with Twitter profiles and posts.
 * When new target person is added adapter starts monitor it's Twitter account.
 * When new fake post is detected adapter sends warning reply to the post author.
 *
 * @author AGeranin (First Line Software)
 */
@Component
@Transactional
public class TwitterAdapter {

    private final Logger logger = LoggerFactory.getLogger(TwitterAdapter.class);

    @Value("${twitter.adapter.posts.queue.read.timeout:300}")
    private long postsQueueReadTimeout;
    @Value("${twitter.adapter.disabled:false}")
    private Boolean disabled;
    @Value("${twitter.adapter.get.history.on.start:true}")
    private Boolean getHistoryOnStart;
    @Value("${twitter.target.group.followers.count.lower.bound:5000}")
    private int targetGroupFollowersCountLowerBound;

    @Autowired
    private TwitterClient twitterClient;
    @Autowired
    private TwitterPostProcessor twitterPostProcessor;
    @Autowired
    private TargetPersonRepository targetPersonRepository;
    @Autowired
    private SentWarningRepository sentWarningRepository;

    private Queue<Post> newPosts;

    private boolean stop;

    @PostConstruct
    private void init() {
        if (disabled) {
            return;
        }
        if (getHistoryOnStart) {
            Index index = new Index();
            List<TargetPerson> targetPersons = Lists.newArrayList(targetPersonRepository.findAll());
            int targetPersonsCount = targetPersons.size();
            targetPersons.forEach(targetPerson -> {
                getAndProcessHistory(targetPerson);
                logger.info("History checked for " + index.incrementAndGet() + " of " + targetPersonsCount);
            });
        }
        newPosts = twitterClient.startMonitoring();
        new Thread(() -> {
            try {
                while (true) {
                    Thread.sleep(postsQueueReadTimeout);
                    if (stop) {
                        logger.info("Stop flag raised. Stopping.");
                        break;
                    }
                    while (!newPosts.isEmpty()) {
                        twitterPostProcessor.process(newPosts.poll());
                    }
                }
            } catch (InterruptedException e) {
                logger.error("Twitter adapter was interrupted!", e);
            }
        }).start();
    }

    /**
     * Gets new target person from event, subscribes platform twitter account to it,
     * gets all available tweets of this target person and processes them
     */
    @Async
    @EventListener(NewTargetPersonAddedEvent.class)
    public void onNewTargetPersonAdded(NewTargetPersonAddedEvent event) {
        if (event.isNew()) {
            TargetPerson newPerson = event.getTargetPerson();
            twitterClient.subscribe(newPerson);
            getAndProcessHistory(newPerson);
        }
    }

    private void getAndProcessHistory(TargetPerson targetPerson) {
        List<Post> newPosts = twitterClient.getHistory(targetPerson);
        newPosts.forEach(post -> post.setOwner(targetPerson));
        twitterPostProcessor.process(newPosts);
    }

    /**
     * Gets new post containing fake news item url and sends warning reply if this posts is from Twitter
     */
    @Async
    @EventListener(FakePostDetectedEvent.class)
    public void onNewFakePost(FakePostDetectedEvent event) {
        logger.debug("New fake post event caught! " + event);
        Post fakePost = event.getSource();
        if (notFromTwitter(fakePost) || warningAlreadySent(fakePost) || ownerNotInTargetGroup(fakePost)) {
            logger.debug("Skipping new fake post: " + fakePost);
            return;
        }
        twitterClient.sendFakeItemWarningReply(fakePost);

        sentWarningRepository.save(new SentWarning(fakePost));

        logger.debug("Warning tweet is sent!");
    }

    private boolean notFromTwitter(Post fakePost) {
        return fakePost.getSocialMedia() != SocialMedia.TWITTER;
    }

    private boolean warningAlreadySent(Post fakePost) {
        return sentWarningRepository.findByPost(fakePost) != null;
    }

    private boolean ownerNotInTargetGroup(Post post) {
        int followersCount = twitterClient.getFollowersCount(post.getOwner());
        return followersCount < targetGroupFollowersCountLowerBound;
    }
}
