package com.credibleu.service.adapter.twitter;

import com.credibleu.domain.Post;
import com.credibleu.domain.TargetPerson;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.stereotype.Component;
import twitter4j.*;
import twitter4j.conf.Configuration;
import twitter4j.conf.ConfigurationBuilder;

import javax.annotation.PostConstruct;
import javax.validation.constraints.NotNull;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;

/**
 * Twitter API client.
 * Incapsulates logic for working with Twitter API.
 *
 * @author AGeranin (First Line Software)
 */
@Component
public class TwitterClient {

    @Value("${twitter.adapter.posts.queue.read.timeout:300}")
    private long postsQueueReadTimeout;

    @Autowired
    private MessageSource messageSource;

    private boolean stop;
    private Configuration twitterConfig;

    private final Logger logger = LoggerFactory.getLogger(TwitterClient.class);

    @PostConstruct
    private void init() {
        ConfigurationBuilder cb = new ConfigurationBuilder();
        cb.setDebugEnabled(true).setOAuthConsumerKey("NYvNqHjj7jz3pRJJSJK9uJWza")
            .setOAuthConsumerSecret("J3FNJMuV3LIB1Cu8qIxmTyoyNG3GG4Id4Pcvb8UUqSGmd1rQ5H")
            .setOAuthAccessToken("845265108423118848-0WnH4YSAOvfdvpSI9CkzjhHFC8QbU34")
            .setOAuthAccessTokenSecret("zxxiIouHQKl4by077gyWlY8gFSEqlgyemcMY8EX8iXpFp");
        twitterConfig = cb.build();
    }

    /**
     * Reads all available tweets of a target person
     */
    public List<Post> getHistory(@NotNull TargetPerson targetPerson) {
        logger.debug("Getting history of " + targetPerson);
        List<Post> posts = new ArrayList<Post>();
        Twitter twitter = new TwitterFactory(twitterConfig).getInstance();
        List<Status> statuses;
        int page = 1;
        if (targetPerson.getLastTwitterPostId() == null) {
            statuses = getUserPosts(twitter, targetPerson.getTwitterId(), new Paging(1, 200));
            while (!statuses.isEmpty()) {
                for (Status status : statuses) {
                    Post post = getPost(status);
                    posts.add(post);
                }
                page++;
                statuses = getUserPosts(twitter, targetPerson.getTwitterId(), new Paging(page, 200));
            }
        } else {
            statuses = getUserPosts(twitter, targetPerson.getTwitterId(), new Paging(1, 200, targetPerson.getLastTwitterPostId()));
            while (!statuses.isEmpty()) {
                for (Status status : statuses) {
                    Post post = getPost(status);
                    posts.add(post);
                }
                page++;
                statuses = getUserPosts(twitter, targetPerson.getTwitterId(), new Paging(page, 200, targetPerson.getLastTwitterPostId()));
            }
        }
        return posts;
    }

    /**
     * Start reading new posts of a target person and sending them to the returned queue
     */
    public Queue<Post> startMonitoring() {

        BlockingQueue<Post> postsQueue = new LinkedBlockingQueue<Post>(10000);

        TwitterStream twitterStream = new TwitterStreamFactory(twitterConfig).getInstance();
        UserStreamAdapter listener = new UserStreamAdapter() {
            @Override
            public void onStatus(Status status) {
                Post post = getPost(status);
                logger.info("New status!", status.getText());
                postsQueue.add(post);
            }

            @Override
            public void onException(Exception ex) {
                logger.error("Stream exception", ex);
            }
        };
        twitterStream.addListener(listener);
        twitterStream.user();

        return postsQueue;
    }

    /**
     * Makes platform twitter account follow specified target person
     */
    public void subscribe(TargetPerson targetPerson) {
        logger.debug("Subscribing to " + targetPerson);
        Twitter twitter = new TwitterFactory(twitterConfig).getInstance();
        try {
            twitter.createFriendship(targetPerson.getTwitterId());
        } catch (TwitterException e) {
            e.printStackTrace();
        }
    }

    /**
     * Downloads twitter avatar of a target person and stores it to show on GUI
     */
    public void setPhoto(TargetPerson targetPerson) {
        logger.debug("Getting photo of " + targetPerson);
        Twitter twitter = new TwitterFactory(twitterConfig).getInstance();
        try {
            User newFriend = twitter.users().showUser(targetPerson.getTwitterId());
            targetPerson.setPhoto(newFriend.getProfileImageURL());
        } catch (TwitterException e) {
            e.printStackTrace();
        }
    }

    /**
     * Converts twitter API Status entity to the platform internal entity Post
     */
    private Post getPost(Status status) {
        Post post = new Post();
        post.setUniqueId(String.valueOf(status.getId()));
        post.setText(status.getText());
        StringBuilder url = new StringBuilder();
        if (status.getURLEntities() != null && status.getURLEntities().length != 0) {
            url.append("[");
            for (int i = 0; i < status.getURLEntities().length; i++) {
                url.append("\"");
                url.append(status.getURLEntities()[i].getExpandedURL());
                url.append("\"");
                url.append(",");
            }
            url.setCharAt(url.lastIndexOf(","), ']');
        }
        post.setUrls(url.toString());
        post.setDatePosted(status.getCreatedAt());
        post.setOwnerTwitterId(String.valueOf(status.getUser().getScreenName()));
        return post;
    }

    /**
     * Gets all twitter account followed by the specified screen name
     */
    public List<TargetPerson> getFollowings(String screenName) {
        Twitter twitter = new TwitterFactory(twitterConfig).getInstance();
        List<User> friends = new LinkedList<>();
        PagableResponseList<User> friendsPage = getFriendsPage(twitter, screenName, -1);
        while (!friendsPage.isEmpty()) {
            friends.addAll(friendsPage);
            friendsPage = getFriendsPage(twitter, screenName, friendsPage.getNextCursor());
        }
        return convertToTargetPersons(friends);
    }

    /**
     * Gets next page of a list of the twitter account specified by the screenName
     */
    private PagableResponseList<User> getFriendsPage(Twitter twitter, String screenName, long cursor) {
        while (true) {
            try {
                return twitter.getFriendsList(screenName, cursor);
            } catch (TwitterException e) {
                logger.info("API limit reached. Waiting for 16 minutes...");
                try {
                    Thread.sleep(16 * 60 * 1000);
                } catch (InterruptedException ie) {
                    logger.error("Interrupted while waiting for API...", ie);
                }
            }
        }
    }

    /**
     * Gets next page of the specified twitter account
     */
    private List<Status> getUserPosts(Twitter twitter, String screenName, Paging paging) {
        while (true) {
            try {
                return twitter.getUserTimeline(screenName, paging);
            } catch (TwitterException e) {
                logger.info("API limit reached. Waiting for 16 minutes...");
                try {
                    Thread.sleep(16 * 60 * 1000);
                } catch (InterruptedException ie) {
                    logger.error("Interrupted while waiting for API...", ie);
                }
            }
        }
    }

    /**
     * Converts a list of twitter entitites User to a list of internal entities TargetPerson
     */
    private List<TargetPerson> convertToTargetPersons(List<User> friends) {
        List<TargetPerson> friendsDTOs = new LinkedList<>();
        for (User user : friends) {
            TargetPerson person = new TargetPerson();
            person.setName(user.getName());
            person.setTwitterId(user.getScreenName());
            friendsDTOs.add(person);
        }
        return friendsDTOs;
    }

    /**
     * Sends reply to the specified Post with a message warning that post contains fake news item.
     */
    public void sendFakeItemWarningReply(Post post) {
        String postOwnerScreenName = post.getOwner().getTwitterId();
        String message = messageSource.getMessage("url.is.fake.warning.tweet", new Object[]{"@" + postOwnerScreenName}, LocaleContextHolder.getLocale());
        sendReply(Long.valueOf(post.getUniqueId()), message);
    }

    /**
     * Sends reply with the specified text to the specified tweet
     */
    private void sendReply(long inReplyToTweetId, String replyText) {
        logger.debug("Sending tweet '" + replyText + "' in reply to a tweet " + inReplyToTweetId);
        Twitter twitter = new TwitterFactory(twitterConfig).getInstance();

        StatusUpdate reply = new StatusUpdate(replyText);
        reply.setInReplyToStatusId(inReplyToTweetId);

        int triesCounter = 0;
        while (triesCounter < 3) {
            try {
                twitter.updateStatus(reply);
                logger.debug("Tweet has been sent!");
                return;
            } catch (TwitterException e) {
                triesCounter++;
                logger.warn("Exception when trying to send reply to tweet " + inReplyToTweetId + ". Tries count: " + triesCounter + ".", e);
                try {
                    Thread.sleep(5 * 1000);
                    logger.info("Trying to send reply again...");
                } catch (InterruptedException ie) {
                    logger.error("Interrupted when waiting for another try to send reply.", ie);
                }
            }
        }
        logger.error("Unable to send reply to tweet " + inReplyToTweetId + "!");
    }

    /**
     * Gets number of followers of the specified target person
     */
    public int getFollowersCount(TargetPerson targetPerson) {
        Twitter twitter = new TwitterFactory(twitterConfig).getInstance();
        try {
            ResponseList<User> users = twitter.lookupUsers(new String[]{targetPerson.getTwitterId()});
            return users.isEmpty() ? 0 : users.get(0).getFollowersCount();
        } catch (TwitterException e) {
            logger.error("Exception when trying to lookup user " + targetPerson.getTwitterId(), e);
            return 0;
        }
    }
}
