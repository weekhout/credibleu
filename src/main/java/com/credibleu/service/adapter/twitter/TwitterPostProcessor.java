package com.credibleu.service.adapter.twitter;

import com.credibleu.domain.Post;
import com.credibleu.domain.TargetPerson;
import com.credibleu.service.adapter.NewPostProcessor;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

/**
 * Implementation of a NewPostProcessor which works with new Twitter posts.
 *
 * @author AGeranin (First Line Software)
 */
@Component
@Transactional
public class TwitterPostProcessor extends NewPostProcessor {

    /**
     * Checks and stores new value of the last twitter post of a target person.
     */
    @Override
    protected void performAdditionalProcessing(Post post) {
        TargetPerson owner = post.getOwner();
        Long tweetId = Long.valueOf(post.getUniqueId());
        if (owner.getLastTwitterPostId() == null || tweetId > owner.getLastTwitterPostId()) {
            owner.setLastTwitterPostId(tweetId);
            targetPersonRepository.save(owner);
        }
    }
}
