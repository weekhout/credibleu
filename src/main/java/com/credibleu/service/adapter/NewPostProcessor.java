package com.credibleu.service.adapter;

import com.credibleu.dao.PostRepository;
import com.credibleu.dao.TargetPersonRepository;
import com.credibleu.domain.Post;
import com.credibleu.domain.event.PostAddedEvent;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEventPublisher;

/**
 * Abstract posts processor defining main steps of post handling process.
 * Post processor first should check if it can skip message:
 * if it's already exists on the platform or it contains no URLs
 * or tweet sender doesn't exist on the platform.
 * Then it should save post, perform any additional processing
 * and finally publish application event that new post obtained.
 */
public abstract class NewPostProcessor {

    private final Logger logger = LoggerFactory.getLogger(NewPostProcessor.class);

    @Autowired
    protected TargetPersonRepository targetPersonRepository;
    @Autowired
    protected PostRepository postRepository;
    @Autowired
    private ApplicationEventPublisher applicationEventPublisher;

    public void process(Iterable<Post> newPosts) {
        newPosts.forEach(this::process);
    }

    public void process(Post post) {
        if (shouldSkip(post)) {
            logger.debug("Skipping post: " + post);
            return;
        }
        if (post.getOwner() == null) {
            post.setOwner(targetPersonRepository.findByTwitterId(post.getOwnerTwitterId()));
        }

        postRepository.save(post);
        logger.debug("Post saved: " + post);

        performAdditionalProcessing(post);

        applicationEventPublisher.publishEvent(new PostAddedEvent(post));
    }

    protected boolean shouldSkip(Post post) {
        return noUrls(post) || ownerNotExists(post) || alreadyExists(post);
    }

    protected boolean noUrls(Post post) {
        return StringUtils.isBlank(post.getUrls());
    }

    protected boolean alreadyExists(Post post) {
        return postRepository.existsWithUniqueId(post.getUniqueId());
    }

    protected boolean ownerNotExists(Post post) {
        return post.getOwner() == null
            && targetPersonRepository.findByTwitterId(post.getOwnerTwitterId()) == null;
    }

    protected abstract void performAdditionalProcessing(Post post);
}
