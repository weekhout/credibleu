package com.credibleu.service;

import com.credibleu.dao.AccountRepository;
import com.credibleu.dao.RatingRepository;
import com.credibleu.domain.Account;
import com.credibleu.domain.FakeItem;
import com.credibleu.domain.Rating;
import com.credibleu.domain.TargetPerson;
import com.credibleu.domain.event.AccountAddedEvent;
import com.credibleu.domain.event.FakeItemAddedEvent;
import com.credibleu.domain.event.NewTargetPersonAddedEvent;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.context.annotation.Scope;
import org.springframework.context.annotation.ScopedProxyMode;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.PostConstruct;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * @author AGeranin (First Line Software)
 */
@Service
@Transactional
@Scope(proxyMode = ScopedProxyMode.TARGET_CLASS)
public class AccountService implements UserDetailsService {

    public static final String ANONYMOUS_USERNAME = "ANONYMOUS";
    private Account anonymousAccountMock;

    @Autowired
    private AccountRepository accountRepository;
    @Autowired
    private PasswordEncoder passwordEncoder;
    @Autowired
    private ApplicationEventPublisher applicationEventPublisher;
    @Autowired
    private SessionStore sessionStore;
    @Autowired
    private RatingRepository ratingRepository;

    @PostConstruct
    private void init() {
        Account anonymousAccount = findByUsername(ANONYMOUS_USERNAME);
        if (anonymousAccount == null) {
            throw new IllegalStateException("Anonymous account isn't created. Create it before starting the application!");
        }
        this.anonymousAccountMock = new Account();
        this.anonymousAccountMock.setId(anonymousAccount.getId());
        this.anonymousAccountMock.setUsername(anonymousAccount.getUsername());
    }

    /**
     * Returns mock account which has a default connected lists of target persons and fake items.
     * It is used to show results without any authentication.
     */
    public Account getAnonymousAccountMock() {
        return anonymousAccountMock;
    }

    public Account add(Account account) {
        account.setPassword(passwordEncoder.encode(account.getPassword()));
        account.setDateCreated(new Date());
        accountRepository.save(account);

        applicationEventPublisher.publishEvent(new AccountAddedEvent(account));

        return account;
    }

    public Map<String, String> validate(Account account) {
        Map<String, String> errors = new HashMap<>();
        String username = account.getUsername();
        if (StringUtils.isBlank(username)) {
            errors.put("username", "Username must not be empty");
        }
        if (StringUtils.isBlank(account.getPassword())) {
            errors.put("password", "Password must not be empty");
        }
        if (!StringUtils.isBlank(username) && accountRepository.usernameExists(username)) {
            errors.put("username", "Username already exists");
        }

        return errors;
    }

    public Account findByUsername(String username) throws UsernameNotFoundException {
        if (StringUtils.isBlank(username)) {
            return null;
        }
        return accountRepository.findByUsername(username);
    }

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        Account account = findByUsername(username);

        if (account == null) {
            throw new UsernameNotFoundException(username);
        }

        return new User(account.getUsername(), account.getPassword(), Collections.<GrantedAuthority>singletonList(() -> "USER"));
    }

    public void addNewFakeItemToCurrentAccount(FakeItem fakeItem) {
        Account owner = accountRepository.findOne(sessionStore.getAccountMock().getId());
        if (owner.getFakeItems().contains(fakeItem)) {
            return;
        }
        owner.getFakeItems().add(fakeItem);
        accountRepository.save(owner);

        applicationEventPublisher.publishEvent(new FakeItemAddedEvent(fakeItem, owner));
    }

    public void addTargetPersonToCurrentAccount(TargetPerson targetPerson) {
        Account owner = accountRepository.findOne(sessionStore.getAccountMock().getId());
        if (owner.getTargetPersons().contains(targetPerson)) {
            return;
        }
        owner.getTargetPersons().add(targetPerson);
        accountRepository.save(owner);

        Rating rating = new Rating();
        rating.setTargetPerson(targetPerson);
        rating.setOwner(owner);
        ratingRepository.save(rating);

        boolean isNew = targetPerson.getOwners() == null;
        applicationEventPublisher.publishEvent(new NewTargetPersonAddedEvent(targetPerson, owner, isNew));
    }
}
