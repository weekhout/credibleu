package com.credibleu.service;

import com.credibleu.domain.Account;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.context.annotation.ScopedProxyMode;
import org.springframework.stereotype.Component;
import org.springframework.web.context.WebApplicationContext;

/**
 * Simple store containing data of a currently logged account.
 *
 * @author AGeranin (First Line Software)
 */
@Component
@Scope(scopeName = WebApplicationContext.SCOPE_SESSION, proxyMode = ScopedProxyMode.TARGET_CLASS)
public class SessionStore {

    @Autowired
    private AccountService accountService;

    private Account accountMock;

    public Account getAccountMock() {
        if (accountMock == null) {
            accountMock = accountService.getAnonymousAccountMock();
        }
        return accountMock;
    }

    public void setAccountMock(Account accountMock) {
        this.accountMock = accountMock;
    }
}
