package com.credibleu.service;

import com.credibleu.domain.Account;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * @author AGeranin (First Line Software)
 */
@Service
@Transactional
public class SecurityService {

    @Autowired
    private AuthenticationManager authenticationManager;
    @Autowired
    private AccountService accountService;
    @Autowired
    private SessionStore sessionStore;

    public void login(String username, String password) {
        UsernamePasswordAuthenticationToken authenticationToken = new UsernamePasswordAuthenticationToken(username, password);
        Authentication authentication = authenticationManager.authenticate(authenticationToken);

        if (authentication.isAuthenticated()) {
            SecurityContextHolder.getContext().setAuthentication(authentication);
            setCurrentAccountToSessionStore(username);
        }
    }

    private void setCurrentAccountToSessionStore(String username) {
        Account account = accountService.findByUsername(username);
        Account accountMock = new Account();
        accountMock.setId(account.getId());
        accountMock.setUsername(account.getUsername());
        sessionStore.setAccountMock(accountMock);
    }
}
