package com.credibleu.service.fakeitems;

import com.credibleu.dao.FakeItemRepository;
import com.credibleu.domain.FakeItem;
import com.credibleu.domain.FakeItemsSource;
import com.credibleu.domain.event.FakeItemAddedEvent;
import com.credibleu.service.AccountService;
import com.credibleu.service.SessionStore;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.PostConstruct;
import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * @author AGeranin (First Line Software)
 */
@Service
@Transactional
public class FakeItemsService {

    @Autowired
    private ApplicationContext applicationContext;
    @Autowired
    private FakeItemRepository fakeItemRepository;
    @Autowired
    private ApplicationEventPublisher applicationEventPublisher;
    @Autowired
    private FakeItemsCache fakeItemsCache;
    @Autowired
    private SessionStore sessionStore;
    @Autowired
    private AccountService accountService;

    private final Map<FakeItemsSource, FakeItemsFileParser> parsers = new HashMap<>();

    @PostConstruct
    private void init() {
        applicationContext.getBeansOfType(FakeItemsFileParser.class)
            .values()
            .forEach(parser -> parsers.put(parser.getSupportedSourceType(), parser));
    }

    public FakeItem create(FakeItem fakeItem) {
        FakeItem existingFakeItem = fakeItemRepository.findByUrl(fakeItem.getUrl().trim().toLowerCase());

        if (existingFakeItem != null) {
            fakeItem = existingFakeItem;
        } else {
            fakeItem.setUrl(fakeItem.getUrl().trim().toLowerCase());
            fakeItem.setSource(FakeItemsSource.MANUAL);
            fakeItem = fakeItemRepository.save(fakeItem);
        }
        accountService.addNewFakeItemToCurrentAccount(fakeItem);
        return fakeItem;
    }

    /**
     * Parses input file with fake news items and stores them
     */
    @SuppressWarnings("unchecked")
    public void parseAndAdd(InputStream fakeNewsFile, FakeItemsSource source) throws IOException {
        FakeItemsFileParser parser = parsers.get(source);
        if (parser == null) {
            throw new IllegalArgumentException("Unsupported fake items source: " + source);
        }
        List<FakeItem> fakeItems = parser.parse(fakeNewsFile);
        List<String> existingFakeUrls = fakeItemsCache.getFakeUrls(sessionStore.getAccountMock());
        fakeItems = fakeItems.stream()
            .filter(fakeItem -> !existingFakeUrls.contains(fakeItem.getUrl().toLowerCase()))
            .collect(Collectors.toList());

        fakeItems.forEach(fakeItem -> {
            fakeItem.setUrl(fakeItem.getUrl().toLowerCase());
            fakeItem.setSource(source);
            applicationEventPublisher.publishEvent(new FakeItemAddedEvent(fakeItem, sessionStore.getAccountMock()));
        });

        fakeItemRepository.save(fakeItems);
    }

    public Page<FakeItem> findAll(Pageable pageable) {
        return fakeItemRepository.findAllByOwners(sessionStore.getAccountMock(), pageable);
    }

    public Page<FakeItem> findByUrlLike(String url, Pageable pageable) {
        return fakeItemRepository.findByUrlLike(sessionStore.getAccountMock(), url.toLowerCase(), pageable);
    }
}
