package com.credibleu.service.fakeitems;

import com.credibleu.dao.AccountRepository;
import com.credibleu.dao.FakeItemRepository;
import com.credibleu.domain.Account;
import com.credibleu.domain.FakeItem;
import com.credibleu.domain.event.AccountAddedEvent;
import com.credibleu.domain.event.FakeItemAddedEvent;
import com.credibleu.dto.FakeItemDTO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.event.EventListener;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.transaction.support.TransactionCallbackWithoutResult;
import org.springframework.transaction.support.TransactionTemplate;

import javax.annotation.PostConstruct;
import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.stream.Collectors;

/**
 * Simple cache storing all fake news items.
 */
@Component
@Transactional
public class FakeItemsCache {

    private Logger logger = LoggerFactory.getLogger(FakeItemsCache.class);

    private final Map<Integer, Set<FakeItemDTO>> fakeItems = new ConcurrentHashMap<>();
    private final Map<Integer, Set<String>> fakeUrls = new ConcurrentHashMap<>();

    @Autowired
    PlatformTransactionManager transactionManager;
    @Autowired
    FakeItemRepository fakeItemRepository;
    @Autowired
    AccountRepository accountRepository;

    @PostConstruct
    private void init() {
        TransactionTemplate transactionTemplate = new TransactionTemplate(transactionManager);
        transactionTemplate.setReadOnly(true);
        transactionTemplate.execute(new TransactionCallbackWithoutResult() {
            @Override
            protected void doInTransactionWithoutResult(TransactionStatus status) {
                accountRepository.findAll().forEach(account -> {
                    Set<FakeItemDTO> fakeItemDTOs = account.getFakeItems().stream()
                        .map(FakeItemDTO::fromDomain)
                        .collect(Collectors.toSet());
                    fakeItems.put(account.getId(), fakeItemDTOs);
                    fakeUrls.put(account.getId(), account.getFakeItems().stream().map(FakeItem::getUrl).collect(Collectors.toSet()));
                });
            }
        });
    }

    @Async
    @EventListener(AccountAddedEvent.class)
    public void onNewAccount(AccountAddedEvent event) {
        Account account = event.getSource();
        fakeItems.put(account.getId(), new HashSet<>());
        fakeUrls.put(account.getId(), new HashSet<>());
    }

    @Async
    @EventListener(FakeItemAddedEvent.class)
    public void onNewFakeItem(FakeItemAddedEvent event) {
        FakeItem newFakeItem = event.getFakeItem();
        Account owner = event.getOwner();
        logger.debug("New fake item added: " + newFakeItem);
        fakeItems.get(owner.getId()).add(FakeItemDTO.fromDomain(newFakeItem));
        fakeUrls.get(owner.getId()).add(newFakeItem.getUrl());
    }

    public List<FakeItemDTO> getFakeItems(Account account) {
        return new ArrayList<>(fakeItems.get(account.getId()));
    }

    public List<String> getFakeUrls(Account account) {
        return new ArrayList<>(fakeUrls.get(account.getId()));
    }
}
