package com.credibleu.service.fakeitems;

import com.credibleu.domain.FakeItemsSource;
import com.credibleu.domain.UncredibleSite;
import org.springframework.boot.json.JsonParser;
import org.springframework.boot.json.JsonParserFactory;
import org.springframework.stereotype.Component;
import org.springframework.util.StreamUtils;

import java.io.IOException;
import java.io.InputStream;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Fake items file parser. Accepts JSON file from
 * https://github.com/OpenSourcesGroup/opensources/blob/master/notCredible/notCredible.json
 * and converts it to a list of UncredibleSite objects.
 *
 * @author AGeranin (First Line Software)
 * @see FakeItemsFileParser
 * @see UncredibleSite
 */
@Component
public class OpenSourcesGroupUncredibleSitesParser implements FakeItemsFileParser<UncredibleSite> {

    @Override
    public FakeItemsSource getSupportedSourceType() {
        return FakeItemsSource.OPEN_SOURCES_GROUP_JSON;
    }

    @Override
    public List<UncredibleSite> parse(InputStream fakeItemsFile) throws IOException {
        String fakeItemsJson = new String(StreamUtils.copyToByteArray(fakeItemsFile));
        JsonParser jsonParser = JsonParserFactory.getJsonParser();
        return jsonParser.parseMap(fakeItemsJson).keySet().stream()
            .map(UncredibleSite::new)
            .collect(Collectors.toList());
    }
}
