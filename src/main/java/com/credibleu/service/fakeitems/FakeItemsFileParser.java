package com.credibleu.service.fakeitems;

import com.credibleu.domain.FakeItem;
import com.credibleu.domain.FakeItemsSource;

import java.io.IOException;
import java.io.InputStream;
import java.util.List;

/**
 * Fake news file parser.
 * Able to read and parse file from the specified FakeItemsSource
 * and convert it's records to a FakeItems collection.
 * <p>
 * Implementations of this interface will be automatically added as file parsers to FakeItemsService
 * and used when necessary.
 *
 * @author AGeranin (First Line Software)
 */
public interface FakeItemsFileParser<T extends FakeItem> {

    FakeItemsSource getSupportedSourceType();

    List<T> parse(InputStream fakeItemsFile) throws IOException;

}
