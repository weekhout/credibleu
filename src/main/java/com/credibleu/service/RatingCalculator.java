package com.credibleu.service;

import com.credibleu.dao.RatingRepository;
import com.credibleu.dao.TargetPersonRepository;
import com.credibleu.domain.*;
import com.credibleu.domain.event.FakeItemAddedEvent;
import com.credibleu.domain.event.FakePostDetectedEvent;
import com.credibleu.domain.event.NewTargetPersonAddedEvent;
import com.credibleu.domain.event.PostAddedEvent;
import com.credibleu.dto.FakeItemDTO;
import com.credibleu.service.fakeitems.FakeItemsCache;
import com.credibleu.utils.Index;
import com.google.common.collect.Lists;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.context.event.EventListener;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.transaction.support.TransactionCallbackWithoutResult;
import org.springframework.transaction.support.TransactionTemplate;

import javax.annotation.PostConstruct;
import java.util.*;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.stream.Collectors;

/**
 * Class calculating target persons 'trust' rating.
 * Each account has it's own fake items list and it's own target persons list.
 * But any target person can be 'owned' by any account.
 * 'Owned' means that this account has this target person in the list of monitored target persons.
 * Such target persons has different ratings for different accounts!
 * <p>
 * Ratings should be recalculated on each new fake news items, new posts and new target persons.
 *
 * @author AGeranin (First Line Software)
 */
@Component
@Transactional
public class RatingCalculator {

    private final Logger logger = LoggerFactory.getLogger(RatingCalculator.class);

    @Value("${rating.calculator.queues.check.timeout:300}")
    private long newQueuesCheckTimeout;

    /**
     * Queue containing all newly arrived posts
     */
    private Queue<Post> newPosts = new ConcurrentLinkedQueue<>();
    /**
     * Queue containing all newly added fake news items.
     * All fake items should have only one owner in the owners collection.
     * It makes it possible to determine who added this fake item.
     */
    private Queue<FakeItem> newFakeItems = new ConcurrentLinkedQueue<>();

    @Autowired
    private TargetPersonRepository targetPersonRepository;
    @Autowired
    private RatingRepository ratingRepository;
    @Autowired
    private FakeItemsCache fakeItemsCache;
    @Autowired
    private PlatformTransactionManager transactionManager;
    @Autowired
    private ApplicationEventPublisher applicationEventPublisher;

    private boolean stop;

    /**
     * Starts thread with infinite loop which checks queues for new incoming data
     * and processes it.
     */
    @PostConstruct
    private void init() {
        new Thread(() -> {
            try {
                while (true) {
                    if (stop) {
                        logger.info("Stop flag raised. Stopping.");
                        break;
                    }
                    if (newPosts.isEmpty() && newFakeItems.isEmpty()) {
                        Thread.sleep(newQueuesCheckTimeout);
                        continue;
                    }
                    if (!newPosts.isEmpty()) {
                        handleNewPosts();
                    }
                    if (!newFakeItems.isEmpty()) {
                        handleNewFakeItems();
                    }
                }
            } catch (InterruptedException e) {
                logger.error("Rating calculator was interrupted during timeout.", e);
            } catch (Exception e) {
                logger.error("Exception in rating calculator!", e);
            }
        }).start();
    }

    /**
     * Gets all available posts from queue and processes them
     */
    private void handleNewPosts() {
        List<Post> allNewPosts = readPostsFromQueue();
        logger.debug("New posts found: " + allNewPosts.size());
        new TransactionTemplate(transactionManager).execute(new TransactionCallbackWithoutResult() {
            @Override
            protected void doInTransactionWithoutResult(TransactionStatus status) {
                recalculateRatingForNewPosts(allNewPosts);
            }
        });
    }

    /**
     * Gets all available posts from queue
     */
    private List<Post> readPostsFromQueue() {
        List<Post> allNewPosts = new ArrayList<>();
        while (!newPosts.isEmpty()) {
            allNewPosts.add(newPosts.poll());
        }
        return allNewPosts;
    }

    /**
     * Gets new posts and recalculates ratings of each target person who sent this post
     * for each account who added this target person to a list of monitored target persons
     * using fake nes items list of this account.
     */
    private void recalculateRatingForNewPosts(List<Post> posts) {
        Set<Post> postsMarkedAsFakeAtLeastOnce = new HashSet<>();
        Map<TargetPerson, List<Post>> personsToPosts = posts.stream().collect(Collectors.groupingBy(Post::getOwner));
        personsToPosts.entrySet().forEach(entry -> {
            TargetPerson postsOwner = entry.getKey();
            List<Post> personsPosts = entry.getValue();
            postsOwner.addTotalPostsCount(personsPosts.size());
            postsOwner = targetPersonRepository.save(postsOwner);

            ratingRepository.findByTargetPerson(postsOwner).forEach(rating -> {
                List<FakeItemDTO> fakeItems = fakeItemsCache.getFakeItems(rating.getOwner());
                int fakePostsCount = countFakePosts(personsPosts, fakeItems);
                rating.addFakePostsCount(fakePostsCount);
                recalculateRating(rating);
                ratingRepository.save(rating);

                if (fakePostsCount != 0) {
                    personsPosts.stream().filter(Post::getIsFake).forEach(postsMarkedAsFakeAtLeastOnce::add);
                }
            });
            logger.debug("Ratings recalculated for " + postsOwner);
        });

        postsMarkedAsFakeAtLeastOnce.stream().map(FakePostDetectedEvent::new).forEach(applicationEventPublisher::publishEvent);
    }

    /**
     * Gets list of posts, list of fake items and 'intersects' them
     * counting how many posts contains fake news urls.
     */
    private int countFakePosts(Collection<Post> posts, Collection<FakeItemDTO> fakeItems) {
        markFakePosts(posts, fakeItems);
        return (int) posts.stream().filter(Post::getIsFake).count();
    }

    /**
     * Checks which posts contains fake items urls and set their isFake flag to true
     */
    public void markFakePosts(Iterable<Post> posts, Collection<FakeItemDTO> fakeItems) {
        List<String> fakeUrls = fakeItems.stream().map(FakeItemDTO::getUrl).map(String::toLowerCase).collect(Collectors.toList());
        posts.forEach(post -> {
            boolean isFake = false;
            for (String fakeUrl : fakeUrls) {
                if (post.getUrls().contains(fakeUrl)) {
                    isFake = true;
                    break;
                }
            }
            post.setIsFake(isFake);
        });
    }

    /**
     * Actualize value of the specified rating.
     */
    public void recalculateRating(Rating rating) {
        rating.setRating(
            calculateRating(
                rating.getTargetPerson().getTotalPostsCount(),
                rating.getFakePostsCount()
            )
        );
    }

    /**
     * Calculate rating of the specified target person for the specified account.
     */
    public void calculateRating(TargetPerson targetPerson, Account account) {
        Rating rating = ratingRepository.findByTargetPersonAndOwner(targetPerson, account);
        Set<Post> posts = rating.getTargetPerson().getPosts();
        List<FakeItemDTO> fakeItems = rating.getOwner().getFakeItems().stream()
            .map(FakeItemDTO::fromDomain)
            .collect(Collectors.toList());

        int fakePostsCount = countFakePosts(posts, fakeItems);
        rating.setFakePostsCount(fakePostsCount);
        recalculateRating(rating);
        ratingRepository.save(rating);

        if (fakePostsCount != 0) {
            posts.stream()
                .filter(Post::getIsFake)
                .map(FakePostDetectedEvent::new)
                .forEach(applicationEventPublisher::publishEvent);
        }
    }

    /**
     * Calculates rating basing on total posts count and number of posts which contains fake news urls.
     */
    public double calculateRating(int totalPostsCount, int fakePostsCount) {
        double rating = (double) fakePostsCount / totalPostsCount;
        return (double) Math.round(rating * 10000) / 10000;
    }

    /**
     * Gets all available fake news items from queue
     * and recalculates ratings for all target persons of account added these fake items.
     */
    private void handleNewFakeItems() {
        List<FakeItem> allNewFakeItems = readFakeItemsFromQueue();
        new TransactionTemplate(transactionManager).execute(new TransactionCallbackWithoutResult() {
            @Override
            protected void doInTransactionWithoutResult(TransactionStatus status) {
                recalculateRatingForNewFakeItems(allNewFakeItems);
            }
        });
    }

    /**
     * Gets all available fake news items from queue
     */
    private List<FakeItem> readFakeItemsFromQueue() {
        List<FakeItem> allNewFakeItems = new ArrayList<>();
        while (!newFakeItems.isEmpty()) {
            allNewFakeItems.add(newFakeItems.poll());
        }
        return allNewFakeItems;
    }

    /**
     * Recalculates rating for all target persons owned by account who added these fake items.
     */
    private void recalculateRatingForNewFakeItems(List<FakeItem> fakeItems) {
        logger.debug("Recalculating ratings for " + fakeItems.size() + " new fake items");

        Set<Post> postsMarkedAsFakeAtLeastOnce = new HashSet<>();

        Map<Account, List<FakeItem>> fakeItemsGroupedByAccount = fakeItems.stream()
            .collect(Collectors.groupingBy(fakeItem -> fakeItem.getOwners().iterator().next()));

        fakeItemsGroupedByAccount.entrySet().forEach(entry -> {
            Account account = entry.getKey();
            List<FakeItemDTO> fakeItemsOfAccount = entry.getValue().stream()
                .map(FakeItemDTO::fromDomain)
                .collect(Collectors.toList());
            List<Rating> ratings = ratingRepository.findByOwner(account);
            ratings.forEach(rating -> {
                Set<Post> ratingPosts = rating.getTargetPerson().getPosts();
                int fakePostsCount = countFakePosts(ratingPosts, fakeItemsOfAccount);
                rating.addFakePostsCount(fakePostsCount);
                recalculateRating(rating);
                ratingRepository.save(rating);

                if (fakePostsCount != 0) {
                    ratingPosts.stream().filter(Post::getIsFake).forEach(postsMarkedAsFakeAtLeastOnce::add);
                }
            });
        });

        postsMarkedAsFakeAtLeastOnce.stream().map(FakePostDetectedEvent::new).forEach(applicationEventPublisher::publishEvent);
    }

    /**
     * Gets new post from event and puts it to the queue with new posts
     */
    @Async
    @EventListener(PostAddedEvent.class)
    public void onNewPost(PostAddedEvent event) {
        newPosts.add(event.getSource());
    }

    /**
     * Gets new fake item from event and puts it to the queue with new fake items
     */
    @Async
    @EventListener(FakeItemAddedEvent.class)
    public void onNewFakeItem(FakeItemAddedEvent event) {
        FakeItem fakeItem = event.getFakeItem();
        fakeItem.setOwners(Collections.singleton(event.getOwner()));
        newFakeItems.add(fakeItem);
    }

    /**
     * Gets new target person from event and calculates rating for it
     */
    @EventListener(NewTargetPersonAddedEvent.class)
    public void onNewTargetPerson(NewTargetPersonAddedEvent event) {
        if (event.isNew()) {
            return;
        }
        calculateRating(event.getTargetPerson(), event.getAccount());
    }

    /**
     * Recalculates all ratings of all target persons.
     */
    public void recalculateAllRatings() {
        List<Rating> ratings = Lists.newArrayList(ratingRepository.findAll());
        int total = ratings.size();
        logger.info("Recalculating " + total + " ratings");
        Index index = new Index();
        ratings.forEach(rating -> {
            TargetPerson targetPerson = rating.getTargetPerson();
            Set<Post> posts = targetPerson.getPosts();
            targetPerson.setTotalPostsCount(posts.size());
            targetPersonRepository.save(targetPerson);

            rating.setFakePostsCount(countFakePosts(
                posts,
                fakeItemsCache.getFakeItems(rating.getOwner())
            ));
            recalculateRating(rating);
            ratingRepository.save(rating);

            logger.info("Recalculated " + index.incrementAndGet() + " of " + total + "...");
        });
    }
}
