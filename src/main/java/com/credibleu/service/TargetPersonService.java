package com.credibleu.service;

import com.credibleu.dao.PostRepository;
import com.credibleu.dao.RatingRepository;
import com.credibleu.dao.TargetPersonRepository;
import com.credibleu.domain.Post;
import com.credibleu.domain.TargetPerson;
import com.credibleu.dto.FakeItemDTO;
import com.credibleu.dto.PostDTO;
import com.credibleu.dto.TargetPersonInfoDTO;
import com.credibleu.service.adapter.twitter.TwitterClient;
import com.credibleu.service.fakeitems.FakeItemsCache;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.stream.Collectors;

/**
 * @author AGeranin (First Line Software)
 */
@Service
@Transactional
public class TargetPersonService {

    @Autowired
    private TargetPersonRepository targetPersonRepository;
    @Autowired
    private TwitterClient twitterClient;
    @Autowired
    private SessionStore sessionStore;
    @Autowired
    private AccountService accountService;
    @Autowired
    private RatingRepository ratingRepository;
    @Autowired
    private PostRepository postRepository;
    @Autowired
    private RatingCalculator ratingCalculator;
    @Autowired
    private FakeItemsCache fakeItemsCache;

    public TargetPerson add(TargetPerson targetPerson) {
        removeFirstAtSign(targetPerson);

        TargetPerson existingPerson = targetPersonRepository.findByTwitterId(targetPerson.getTwitterId());
        if (existingPerson != null) {
            targetPerson = existingPerson;
        } else {
            twitterClient.setPhoto(targetPerson);
            targetPerson = targetPersonRepository.save(targetPerson);
        }
        accountService.addTargetPersonToCurrentAccount(targetPerson);
        return targetPerson;
    }

    /**
     * Adds all twitter accounts followed by the twitter account specified
     * by the sreenName to list of target persons of a current account.
     */
    public void addAllFollowingsOf(String screenName) {
        List<TargetPerson> followings = twitterClient.getFollowings(screenName);
        followings.forEach(this::add);
    }

    private void removeFirstAtSign(TargetPerson targetPerson) {
        if (targetPerson.getTwitterId().startsWith("@")) {
            targetPerson.setTwitterId(targetPerson.getTwitterId().substring(1));
        }
    }

    public Page<TargetPersonInfoDTO> findAll(Pageable paging) {
        return convertToDTOsPage(targetPersonRepository.findAllByOwners(sessionStore.getAccountMock(), paging));
    }

    public Page<TargetPersonInfoDTO> findByNameLike(String name, Pageable paging) {
        return convertToDTOsPage(targetPersonRepository.findByOwnerAndNameLike(sessionStore.getAccountMock(), name.toLowerCase(), paging));
    }

    public Page<TargetPersonInfoDTO> findByTwitterIdLike(String twitterId, Pageable paging) {
        return convertToDTOsPage(targetPersonRepository.findByOwnerAndTwitterIdLike(sessionStore.getAccountMock(), twitterId.toLowerCase(), paging));
    }

    private Page<TargetPersonInfoDTO> convertToDTOsPage(Page<TargetPerson> targetPersons) {
        return targetPersons.map(targetPerson -> TargetPersonInfoDTO.fromDomain(
            targetPerson,
            ratingRepository.findByTargetPersonAndOwner(targetPerson, sessionStore.getAccountMock())
        ));
    }

    public List<PostDTO> getFakePostsOfPersonWithId(Integer personId) {
        TargetPerson mock = new TargetPerson();
        mock.setId(personId);
        List<Post> posts = postRepository.findByOwner(mock);
        List<FakeItemDTO> fakeItems = fakeItemsCache.getFakeItems(sessionStore.getAccountMock());
        ratingCalculator.markFakePosts(posts, fakeItems);
        return posts.stream()
            .filter(Post::getIsFake)
            .map(PostDTO::fromDomain)
            .sorted((o1, o2) -> o2.getDatePosted().compareTo(o1.getDatePosted()))
            .collect(Collectors.toList());
    }
}
