package com.credibleu.utils;

/**
 * Simple utility class to use it as an index in stream().forEach() method.
 *
 * @author AGeranin (First Line Software)
 */
public class Index {

    private long value;

    public long incrementAndGet() {
        value += 1;
        return value;
    }

    public long value() {
        return value;
    }

    public void reset() {
        value = 0;
    }
}
