package com.credibleu.dto;

import com.credibleu.domain.FakeItem;
import com.credibleu.domain.FakeItemsSource;

import java.util.Date;
import java.util.Objects;

/**
 * @author AGeranin (First Line Software)
 */
public class FakeItemDTO {

    protected Integer id;
    protected String url;
    protected Date dateAdded = new Date();
    protected FakeItemsSource source;

    public FakeItemDTO() {
    }

    public FakeItemDTO(Integer id, String url, Date dateAdded, FakeItemsSource source) {
        this.id = id;
        this.url = url;
        this.dateAdded = dateAdded;
        this.source = source;
    }

    public static FakeItemDTO fromDomain(FakeItem fakeItem) {
        return new FakeItemDTO(
            fakeItem.getId(),
            fakeItem.getUrl(),
            fakeItem.getDateAdded(),
            fakeItem.getSource()
        );
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public Date getDateAdded() {
        return dateAdded;
    }

    public void setDateAdded(Date dateAdded) {
        this.dateAdded = dateAdded;
    }

    public FakeItemsSource getSource() {
        return source;
    }

    public void setSource(FakeItemsSource source) {
        this.source = source;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        FakeItemDTO that = (FakeItemDTO) o;
        return Objects.equals(id, that.id) &&
            Objects.equals(url, that.url) &&
            Objects.equals(dateAdded, that.dateAdded) &&
            Objects.equals(source, that.source);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, url, dateAdded, source);
    }
}
