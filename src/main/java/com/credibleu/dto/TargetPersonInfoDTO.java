package com.credibleu.dto;

import com.credibleu.domain.Rating;
import com.credibleu.domain.TargetPerson;

import java.util.Date;

/**
 * @author AGeranin (First Line Software)
 */
public class TargetPersonInfoDTO {

    private Integer id;
    private String name;
    private String twitterId;
    private Date dateAdded = new Date();
    private Long lastTwitterPostId;
    private Integer totalPostsCount = 0;
    private Integer fakePostsCount = 0;
    private Double rating;
    private String photo;

    public TargetPersonInfoDTO(Integer id, String name, String twitterId, Date dateAdded, Long lastTwitterPostId, Integer totalPostsCount, Integer fakePostsCount, Double rating, String photo) {
        this.id = id;
        this.name = name;
        this.twitterId = twitterId;
        this.dateAdded = dateAdded;
        this.lastTwitterPostId = lastTwitterPostId;
        this.totalPostsCount = totalPostsCount;
        this.fakePostsCount = fakePostsCount;
        this.rating = rating;
        this.photo = photo;
    }

    public static TargetPersonInfoDTO fromDomain(TargetPerson targetPerson, Rating rating) {
        return new TargetPersonInfoDTO(
            targetPerson.getId(),
            targetPerson.getName(),
            targetPerson.getTwitterId(),
            targetPerson.getDateAdded(),
            targetPerson.getLastTwitterPostId(),
            targetPerson.getTotalPostsCount(),
            rating.getFakePostsCount(),
            rating.getRating(),
            targetPerson.getPhoto()
        );
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getTwitterId() {
        return twitterId;
    }

    public void setTwitterId(String twitterId) {
        this.twitterId = twitterId;
    }

    public Date getDateAdded() {
        return dateAdded;
    }

    public void setDateAdded(Date dateAdded) {
        this.dateAdded = dateAdded;
    }

    public Long getLastTwitterPostId() {
        return lastTwitterPostId;
    }

    public void setLastTwitterPostId(Long lastTwitterPostId) {
        this.lastTwitterPostId = lastTwitterPostId;
    }

    public Integer getTotalPostsCount() {
        return totalPostsCount;
    }

    public void setTotalPostsCount(Integer totalPostsCount) {
        this.totalPostsCount = totalPostsCount;
    }

    public Integer getFakePostsCount() {
        return fakePostsCount;
    }

    public void setFakePostsCount(Integer fakePostsCount) {
        this.fakePostsCount = fakePostsCount;
    }

    public Double getRating() {
        return rating;
    }

    public void setRating(Double rating) {
        this.rating = rating;
    }

    public String getPhoto() {
        return photo;
    }

    public void setPhoto(String photo) {
        this.photo = photo;
    }
}
