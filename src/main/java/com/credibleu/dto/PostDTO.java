package com.credibleu.dto;

import com.credibleu.domain.Post;

import java.util.Date;

/**
 * @author AGeranin (First Line Software)
 */
public class PostDTO {

    private String text;
    private Date datePosted;
    private Boolean isFake;
    private String urls;

    public PostDTO() {
    }

    public PostDTO(String text, Date datePosted, Boolean isFake, String urls) {
        this.text = text;
        this.datePosted = datePosted;
        this.isFake = isFake;
        this.urls = urls;
    }

    public static PostDTO fromDomain(Post post) {
        return new PostDTO(
            post.getText(),
            post.getDatePosted(),
            post.getIsFake(),
            post.getUrls()
        );
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public Date getDatePosted() {
        return datePosted;
    }

    public void setDatePosted(Date datePosted) {
        this.datePosted = datePosted;
    }

    public Boolean getIsFake() {
        return isFake;
    }

    public void setIsFake(Boolean isFake) {
        this.isFake = isFake;
    }

    public String getUrls() {
        return urls;
    }

    public void setUrls(String urls) {
        this.urls = urls;
    }
}
