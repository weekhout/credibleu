package com.credibleu.dao;

import com.credibleu.domain.Post;
import com.credibleu.domain.SentWarning;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.transaction.annotation.Transactional;

/**
 * @author AGeranin (First Line Software)
 */
@Transactional
public interface SentWarningRepository extends PagingAndSortingRepository<SentWarning, Integer> {

    SentWarning findByPost(Post post);

}
