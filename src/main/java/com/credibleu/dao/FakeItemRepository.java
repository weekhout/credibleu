package com.credibleu.dao;

import com.credibleu.domain.Account;
import com.credibleu.domain.FakeItem;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

/**
 * @author AGeranin (First Line Software)
 */
@Transactional
public interface FakeItemRepository extends FakeItemBaseRepository<FakeItem> {
    @Query("select fi from FakeItem fi inner join fi.owners owner where lower(fi.url) like concat(:url, '%') and owner = :account")
    Page<FakeItem> findByUrlLike(@Param("account") Account account, @Param("url") String url, Pageable pageable);

    FakeItem findByUrl(String url);

    Page<FakeItem> findAllByOwners(Account owner, Pageable pageable);
}
