package com.credibleu.dao;

import com.credibleu.domain.FakeItem;
import org.springframework.data.repository.NoRepositoryBean;
import org.springframework.data.repository.PagingAndSortingRepository;

/**
 * @author AGeranin (First Line Software)
 */
@NoRepositoryBean
public interface FakeItemBaseRepository<T extends FakeItem> extends PagingAndSortingRepository<T, Integer> {
}
