package com.credibleu.dao;

import com.credibleu.domain.Account;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

/**
 * @author AGeranin (First Line Software)
 */
@Transactional
public interface AccountRepository extends PagingAndSortingRepository<Account, Integer> {

    @Query("select count(a) > 0 from Account a where lower(a.username) = lower(:username)")
    boolean usernameExists(@Param("username") String username);

    Account findByUsername(String username);

}
