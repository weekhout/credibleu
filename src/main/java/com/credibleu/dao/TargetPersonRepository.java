package com.credibleu.dao;

import com.credibleu.domain.Account;
import com.credibleu.domain.TargetPerson;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

/**
 * @author AGeranin (First Line Software)
 */
@Transactional
public interface TargetPersonRepository extends PagingAndSortingRepository<TargetPerson, Integer> {

    String RATING_SORT_FIELDS_PREFIX = "r.";

    @Query("select tp from TargetPerson tp " +
        "inner join tp.owners tpowner " +
        "inner join tp.ratings r " +
        "inner join r.owner rowner " +
        "where rowner = :owner and tpowner = :owner")
    Page<TargetPerson> findAllByOwners(@Param("owner") Account owner, Pageable pageable);

    TargetPerson findByTwitterId(String twitterId);

    @Query("select tp from TargetPerson tp " +
        "inner join tp.owners tpowner " +
        "inner join tp.ratings r " +
        "inner join r.owner rowner " +
        "where rowner = :owner and tpowner = :owner " +
        "and lower(tp.name) like concat(:name,'%') " +
        "or lower(tp.name) like concat('% ', :name, '%')")
    Page<TargetPerson> findByOwnerAndNameLike(@Param("owner") Account owner, @Param("name") String name, Pageable pageable);

    @Query("select tp from TargetPerson tp " +
        "inner join tp.owners tpowner " +
        "inner join tp.ratings r " +
        "inner join r.owner rowner " +
        "where rowner = :owner and tpowner = :owner " +
        "and lower(tp.twitterId) like concat(:twitterId, '%')")
    Page<TargetPerson> findByOwnerAndTwitterIdLike(@Param("owner") Account owner, @Param("twitterId") String twitterId, Pageable pageable);
}
