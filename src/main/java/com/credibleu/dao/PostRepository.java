package com.credibleu.dao;

import com.credibleu.domain.Post;
import com.credibleu.domain.TargetPerson;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * @author AGeranin (First Line Software)
 */
@Transactional
public interface PostRepository extends PagingAndSortingRepository<Post, Integer> {
    @Query("select count(p) > 0 from Post p where p.uniqueId = :uniqueId")
    boolean existsWithUniqueId(@Param("uniqueId") String uniqueId);

    List<Post> findByOwner(TargetPerson owner);
}
