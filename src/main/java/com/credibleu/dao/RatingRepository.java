package com.credibleu.dao;

import com.credibleu.domain.Account;
import com.credibleu.domain.Rating;
import com.credibleu.domain.TargetPerson;
import org.springframework.data.repository.PagingAndSortingRepository;

import java.util.List;

/**
 * @author AGeranin (First Line Software)
 */
public interface RatingRepository extends PagingAndSortingRepository<Rating, Integer> {
    List<Rating> findByOwner(Account owner);

    List<Rating> findByTargetPerson(TargetPerson targetPerson);

    Rating findByTargetPersonAndOwner(TargetPerson targetPerson, Account owner);
}
