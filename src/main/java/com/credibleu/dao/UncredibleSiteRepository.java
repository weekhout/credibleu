package com.credibleu.dao;

import com.credibleu.domain.UncredibleSite;
import org.springframework.transaction.annotation.Transactional;

/**
 * @author AGeranin (First Line Software)
 */
@Transactional
public interface UncredibleSiteRepository extends FakeItemBaseRepository<UncredibleSite> {
}
