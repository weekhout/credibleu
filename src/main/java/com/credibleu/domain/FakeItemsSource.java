package com.credibleu.domain;

/**
 * Constants to indicate different fake news sources.
 * This is necessary for choosing corresponding fake news file parser.
 *
 * @author AGeranin (First Line Software)
 */
public enum FakeItemsSource {

    /**
     * For fake news items added manually
     */
    MANUAL,
    /**
     * For fake new items added from the JSON file
     * from Open sources group database
     */
    OPEN_SOURCES_GROUP_JSON

}
