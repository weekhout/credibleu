package com.credibleu.domain;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.Date;
import java.util.Objects;

/**
 * Key platform entity describing post/message published by target person
 * in social networks, blogs or any other sites.
 *
 * @author AGeranin (First Line Software)
 */
@Entity
public class Post {

    @Id
    @SequenceGenerator(name = "pk_sequence", sequenceName = "entity_id_seq")
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "pk_sequence")
    private Integer id;
    /**
     * Unique id of the post on social media
     */
    @NotNull
    private String uniqueId;
    private String text;
    /**
     * List of strings in JSON.
     * URLs to news or all URLs from post
     * if it's not possible to separate them
     */
    @NotNull
    private String urls;
    @NotNull
    private Date datePosted;
    @ManyToOne
    @JoinColumn(name = "owner_id")
    private TargetPerson owner;
    @Enumerated(EnumType.STRING)
    private SocialMedia socialMedia = SocialMedia.TWITTER;
    @NotNull
    private Date dateAdded = new Date();
    @Transient
    private Boolean isFake;
    @Transient
    private String ownerTwitterId;

    public Post() {
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getUniqueId() {
        return uniqueId;
    }

    public void setUniqueId(String uniqueId) {
        this.uniqueId = uniqueId;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String getUrls() {
        return urls;
    }

    public void setUrls(String urls) {
        this.urls = urls;
    }

    public Date getDatePosted() {
        return datePosted;
    }

    public void setDatePosted(Date datePosted) {
        this.datePosted = datePosted;
    }

    public Boolean getIsFake() {
        return isFake;
    }

    public void setIsFake(Boolean isFake) {
        this.isFake = isFake;
    }

    public TargetPerson getOwner() {
        return owner;
    }

    public void setOwner(TargetPerson owner) {
        this.owner = owner;
    }

    public SocialMedia getSocialMedia() {
        return socialMedia;
    }

    public void setSocialMedia(SocialMedia socialMedia) {
        this.socialMedia = socialMedia;
    }

    public Date getDateAdded() {
        return dateAdded;
    }

    public void setDateAdded(Date dateAdded) {
        this.dateAdded = dateAdded;
    }

    public String getOwnerTwitterId() {
        return ownerTwitterId;
    }

    public void setOwnerTwitterId(String ownerTwitterId) {
        this.ownerTwitterId = ownerTwitterId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Post post = (Post) o;
        return Objects.equals(id, post.id) &&
            Objects.equals(datePosted, post.datePosted) &&
            Objects.equals(owner, post.owner) &&
            Objects.equals(dateAdded, post.dateAdded);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, datePosted, owner, dateAdded);
    }

    @Override
    public String toString() {
        return "Post{" +
            "id=" + id +
            ", uniqueId='" + uniqueId + '\'' +
            ", text='" + text + '\'' +
            ", urls='" + urls + '\'' +
            ", datePosted=" + datePosted +
            ", owner=" + owner +
            ", socialMedia=" + socialMedia +
            ", dateAdded=" + dateAdded +
            ", ownerTwitterId='" + ownerTwitterId + '\'' +
            '}';
    }
}
