package com.credibleu.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import java.util.Date;
import java.util.Objects;
import java.util.Set;

/**
 * User account.
 * Account has it's own list of a target persons and it's own list of a fake news items.
 *
 * @author AGeranin (First Line Software)
 */
@Entity
public class Account {

    @Id
    @SequenceGenerator(name = "pk_sequence", sequenceName = "entity_id_seq")
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "pk_sequence")
    private Integer id;

    private String username;
    @JsonIgnore
    private String password;
    private Date dateCreated;
    @JsonIgnore
    @ManyToMany
    @JoinTable(
        name = "account_to_person",
        joinColumns = @JoinColumn(name = "account_id", referencedColumnName = "id"),
        inverseJoinColumns = @JoinColumn(name = "target_person_id", referencedColumnName = "id")
    )
    private Set<TargetPerson> targetPersons;
    @JsonIgnore
    @ManyToMany
    @JoinTable(
        name = "account_to_fake_item",
        joinColumns = @JoinColumn(name = "account_id", referencedColumnName = "id"),
        inverseJoinColumns = @JoinColumn(name = "fake_item_id", referencedColumnName = "id")
    )
    private Set<FakeItem> fakeItems;

    public Account() {
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public Date getDateCreated() {
        return dateCreated;
    }

    public void setDateCreated(Date dateCreated) {
        this.dateCreated = dateCreated;
    }

    public Set<TargetPerson> getTargetPersons() {
        return targetPersons;
    }

    public void setTargetPersons(Set<TargetPerson> targetPersons) {
        this.targetPersons = targetPersons;
    }

    public Set<FakeItem> getFakeItems() {
        return fakeItems;
    }

    public void setFakeItems(Set<FakeItem> fakeItems) {
        this.fakeItems = fakeItems;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Account account = (Account) o;
        return Objects.equals(id, account.id) &&
            Objects.equals(username, account.username) &&
            Objects.equals(dateCreated, account.dateCreated);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, username, dateCreated);
    }

    @Override
    public String toString() {
        return "Account{" +
            "id=" + id +
            ", username='" + username + '\'' +
            ", dateCreated=" + dateCreated +
            '}';
    }
}
