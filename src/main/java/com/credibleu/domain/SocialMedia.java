package com.credibleu.domain;

/**
 * Constants describing different posts sources which platform can monitor.
 * Used to indicate the source of processed post.
 *
 * @author AGeranin (First Line Software)
 */
public enum SocialMedia {

    TWITTER("social.media.twitter");

    private String nameKey;

    SocialMedia(String nameKey) {
        this.nameKey = nameKey;
    }

    public String getNameKey() {
        return nameKey;
    }
}
