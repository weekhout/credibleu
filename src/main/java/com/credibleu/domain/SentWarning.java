package com.credibleu.domain;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.Date;
import java.util.Objects;

/**
 * A message sent to the target person by the platform.
 * Such message warns that news item used by this target person in his post
 * is untrusted.
 *
 * @author AGeranin (First Line Software)
 */
@Entity
public class SentWarning {

    @Id
    @SequenceGenerator(name = "pk_sequence", sequenceName = "entity_id_seq")
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "pk_sequence")
    private Integer id;

    @NotNull
    @OneToOne
    @JoinColumn(name = "post_id")
    private Post post;

    @NotNull
    private Date dateSent = new Date();

    public SentWarning() {
    }

    public SentWarning(Post post) {
        this.post = post;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Post getPost() {
        return post;
    }

    public void setPost(Post post) {
        this.post = post;
    }

    public Date getDateSent() {
        return dateSent;
    }

    public void setDateSent(Date dateSent) {
        this.dateSent = dateSent;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        SentWarning that = (SentWarning) o;
        return Objects.equals(id, that.id) &&
            Objects.equals(post, that.post) &&
            Objects.equals(dateSent, that.dateSent);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, post, dateSent);
    }

    @Override
    public String toString() {
        return "SentWarning{" +
            "id=" + id +
            ", post=" + post +
            ", dateSent=" + dateSent +
            '}';
    }
}
