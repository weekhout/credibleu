package com.credibleu.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.Date;
import java.util.Objects;
import java.util.Set;

/**
 * Key platform entity - Fake News Item.
 * Abstract entity describing a message containing fake/unconfirmed/unproved facts
 * and published somewhere on the Internet
 * or the whole resource containing so many such fake messages
 * that it could be considered untrusted source of an information.
 *
 * @author AGeranin (First Line Software)
 */
@Entity
@Inheritance
public abstract class FakeItem {

    @Id
    @SequenceGenerator(name = "pk_sequence", sequenceName = "entity_id_seq")
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "pk_sequence")
    protected Integer id;
    /**
     * A URL to the
     */
    @NotNull
    protected String url;
    @NotNull
    protected Date dateAdded = new Date();
    /**
     * A way this fake news item was uploaded to the platform
     */
    @Enumerated(EnumType.STRING)
    protected FakeItemsSource source;
    /**
     * Each fake news item may belong to number of accounts in order to avoid data duplication
     */
    @JsonIgnore
    @ManyToMany(mappedBy = "fakeItems")
    protected Set<Account> owners;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public Date getDateAdded() {
        return dateAdded;
    }

    public void setDateAdded(Date dateAdded) {
        this.dateAdded = dateAdded;
    }

    public FakeItemsSource getSource() {
        return source;
    }

    public void setSource(FakeItemsSource source) {
        this.source = source;
    }

    public Set<Account> getOwners() {
        return owners;
    }

    public void setOwners(Set<Account> owners) {
        this.owners = owners;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        FakeItem fakeItem = (FakeItem) o;
        return Objects.equals(id, fakeItem.id) &&
            Objects.equals(url, fakeItem.url) &&
            Objects.equals(dateAdded, fakeItem.dateAdded) &&
            Objects.equals(source, fakeItem.source);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, url, dateAdded, source);
    }
}
