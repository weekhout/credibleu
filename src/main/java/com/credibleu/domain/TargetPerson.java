package com.credibleu.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.Date;
import java.util.Objects;
import java.util.Set;

/**
 * Key platform entity describing a person whose posts platform monitors.
 *
 * @author AGeranin (First Line Software)
 */
@Entity
public class TargetPerson {

    @Id
    @SequenceGenerator(name = "pk_sequence", sequenceName = "entity_id_seq")
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "pk_sequence")
    private Integer id;
    @NotNull
    private String name;
    @NotNull
    private String twitterId;
    @NotNull
    private Date dateAdded = new Date();
    /**
     * Id of a last tweet from this target persons registered by a platform.
     * Needed get skipped tweets of a target person if platform was down for some time.
     */
    private Long lastTwitterPostId;
    private Integer totalPostsCount = 0;
    private String photo;

    @JsonIgnore
    @ManyToMany(mappedBy = "targetPersons")
    private Set<Account> owners;
    @JsonIgnore
    @OneToMany(mappedBy = "targetPerson")
    private Set<Rating> ratings;
    @JsonIgnore
    @OneToMany(mappedBy = "owner")
    private Set<Post> posts;

    public TargetPerson() {
    }

    public TargetPerson(String name, String twitterId) {
        this.name = name;
        this.twitterId = twitterId;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getTwitterId() {
        return twitterId;
    }

    public void setTwitterId(String twitterId) {
        this.twitterId = twitterId;
    }

    public Date getDateAdded() {
        return dateAdded;
    }

    public void setDateAdded(Date dateAdded) {
        this.dateAdded = dateAdded;
    }

    public Long getLastTwitterPostId() {
        return lastTwitterPostId;
    }

    public void setLastTwitterPostId(Long lastTwitterPostId) {
        this.lastTwitterPostId = lastTwitterPostId;
    }

    public Integer getTotalPostsCount() {
        return totalPostsCount;
    }

    public void setTotalPostsCount(Integer totalPostsCount) {
        this.totalPostsCount = totalPostsCount;
    }

    public void addTotalPostsCount(int totalPostsCount) {
        this.totalPostsCount += totalPostsCount;
    }

    public Set<Rating> getRatings() {
        return ratings;
    }

    public void setRatings(Set<Rating> ratings) {
        this.ratings = ratings;
    }

    public Set<Account> getOwners() {
        return owners;
    }

    public void setOwners(Set<Account> owners) {
        this.owners = owners;
    }

    public Set<Post> getPosts() {
        return posts;
    }

    public void setPosts(Set<Post> posts) {
        this.posts = posts;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        TargetPerson that = (TargetPerson) o;
        return Objects.equals(id, that.id) &&
            Objects.equals(name, that.name) &&
            Objects.equals(dateAdded, that.dateAdded);
    }

    public String getPhoto() {
        return photo;
    }

    public void setPhoto(String photo) {
        this.photo = photo;
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, name, dateAdded);
    }

    @Override
    public String toString() {
        return "TargetPerson{" +
            "id=" + id +
            ", name='" + name + '\'' +
            ", twitterId='" + twitterId + '\'' +
            ", dateAdded=" + dateAdded +
            ", lastTwitterPostId=" + lastTwitterPostId +
            ", totalPostsCount=" + totalPostsCount +
            '}';
    }
}
