package com.credibleu.domain;

import javax.persistence.Entity;

/**
 * Fake news item implementation describing a site with fake news.
 *
 * @author AGeranin (First Line Software)
 */
@Entity
public class UncredibleSite extends FakeItem {

    public UncredibleSite() {
    }

    public UncredibleSite(String url) {
        this.url = url;
    }
}
