package com.credibleu.domain.event;

import com.credibleu.domain.Account;
import com.credibleu.domain.TargetPerson;
import org.springframework.context.ApplicationEvent;

/**
 * Thrown when new target person has been added to the platform
 */
public class NewTargetPersonAddedEvent extends ApplicationEvent {

    private TargetPerson targetPerson;
    private Account account;
    private boolean isNew = false;

    public NewTargetPersonAddedEvent(TargetPerson newPerson, Account account, boolean isNew) {
        super(newPerson);
        this.targetPerson = newPerson;
        this.account = account;
        this.isNew = isNew;
    }

    public TargetPerson getTargetPerson() {
        return targetPerson;
    }

    public Account getAccount() {
        return account;
    }

    public boolean isNew() {
        return isNew;
    }
}
