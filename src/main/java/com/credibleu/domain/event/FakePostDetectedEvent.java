package com.credibleu.domain.event;

import com.credibleu.domain.Post;
import org.springframework.context.ApplicationEvent;

/**
 * Thrown when new post containing fake news item URL
 * has been detected in target person's posts
 */
public class FakePostDetectedEvent extends ApplicationEvent {

    public FakePostDetectedEvent(Post source) {
        super(source);
    }

    @Override
    public Post getSource() {
        return (Post) super.getSource();
    }
}
