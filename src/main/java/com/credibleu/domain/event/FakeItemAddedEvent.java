package com.credibleu.domain.event;

import com.credibleu.domain.Account;
import com.credibleu.domain.FakeItem;
import org.springframework.context.ApplicationEvent;

/**
 * Thrown when new fake news item has been added to the platform
 */
public class FakeItemAddedEvent extends ApplicationEvent {

    private FakeItem fakeItem;
    private Account owner;

    public FakeItemAddedEvent(FakeItem fakeItem, Account owner) {
        super(fakeItem);
        this.fakeItem = fakeItem;
        this.owner = owner;
    }

    public FakeItem getFakeItem() {
        return fakeItem;
    }

    public Account getOwner() {
        return owner;
    }
}
