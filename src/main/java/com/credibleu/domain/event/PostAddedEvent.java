package com.credibleu.domain.event;

import com.credibleu.domain.Post;
import org.springframework.context.ApplicationEvent;

/**
 * Thrown when new post of a target person has been detected
 */
public class PostAddedEvent extends ApplicationEvent {

    public PostAddedEvent(Post newPost) {
        super(newPost);
    }

    @Override
    public Post getSource() {
        return (Post) super.getSource();
    }
}
