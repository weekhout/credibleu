package com.credibleu.domain.event;

import com.credibleu.domain.Account;
import org.springframework.context.ApplicationEvent;

/**
 * Thrown when new account has been added to the platform
 */
public class AccountAddedEvent extends ApplicationEvent {

    public AccountAddedEvent(Account source) {
        super(source);
    }

    @Override
    public Account getSource() {
        return (Account) super.getSource();
    }
}
