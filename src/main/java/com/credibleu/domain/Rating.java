package com.credibleu.domain;

import com.google.common.collect.Lists;

import javax.persistence.*;
import java.util.List;
import java.util.Objects;

/**
 * Rating of a target person, showing how much people can trust his posts.
 * It is calculated basing on total number of target person's posts
 * and number of posts containing fake news items.
 *
 * @author AGeranin (First Line Software)
 */
@Entity
public class Rating {

    public static List<String> RATING_SORT_FIELDS = Lists.newArrayList(
        "fakePostsCount",
        "rating"
    );

    @Id
    @SequenceGenerator(name = "pk_sequence", sequenceName = "entity_id_seq")
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "pk_sequence")
    private Integer id;
    @ManyToOne
    @JoinColumn(name = "target_person_id")
    private TargetPerson targetPerson;

    private Integer fakePostsCount = 0;
    private Double rating;
    @ManyToOne
    @JoinColumn(name = "owner_id")
    private Account owner;

    public Rating() {
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public TargetPerson getTargetPerson() {
        return targetPerson;
    }

    public void setTargetPerson(TargetPerson targetPerson) {
        this.targetPerson = targetPerson;
    }

    public Integer getFakePostsCount() {
        return fakePostsCount;
    }

    public void setFakePostsCount(Integer fakePostsCount) {
        this.fakePostsCount = fakePostsCount;
    }

    public Double getRating() {
        return rating;
    }

    public void setRating(Double rating) {
        this.rating = rating;
    }

    public Account getOwner() {
        return owner;
    }

    public void setOwner(Account owner) {
        this.owner = owner;
    }

    public void addFakePostsCount(int fakePostsCount) {
        this.fakePostsCount += fakePostsCount;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Rating rating1 = (Rating) o;
        return Objects.equals(id, rating1.id) &&
            Objects.equals(rating, rating1.rating);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, rating);
    }

    @Override
    public String toString() {
        return "Rating{" +
            "id=" + id +
            ", targetPerson=" + targetPerson +
            ", fakePostsCount=" + fakePostsCount +
            ", rating=" + rating +
            ", owner=" + owner +
            '}';
    }
}
