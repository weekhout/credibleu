package com.credibleu.controller;

import com.credibleu.dao.TargetPersonRepository;
import com.credibleu.domain.Rating;
import com.credibleu.domain.TargetPerson;
import com.credibleu.dto.JTableDTO;
import com.credibleu.dto.JTableRecordDTO;
import com.credibleu.dto.TargetPersonInfoDTO;
import com.credibleu.service.TargetPersonService;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

/**
 * @author AGeranin (First Line Software)
 */
@Controller
public class TargetPersonController extends JTableController {

    private final Logger logger = LoggerFactory.getLogger(TargetPersonController.class);

    @Autowired
    private TargetPersonService targetPersonService;

    /**
     * Adds all of the specified Twitter account followers to the platform
     * as a target persons
     */
    @ResponseBody
    @RequestMapping(value = "/targetPersons/addFollowings", method = RequestMethod.POST)
    public void addFollowings(String screenName) {
        targetPersonService.addAllFollowingsOf(screenName);
    }

    @ResponseBody
    @RequestMapping(value = "/targetPerson", method = RequestMethod.POST)
    public JTableRecordDTO createTargetPerson(
        @RequestParam String name,
        @RequestParam String twitterId
    ) {
        JTableRecordDTO result = new JTableRecordDTO();
        try {
            TargetPerson targetPerson = new TargetPerson(name, twitterId);
            targetPerson = targetPersonService.add(targetPerson);
            result.Result = "OK";
            result.Record = targetPerson;
        } catch (Exception e) {
            result.Result = "ERROR";
            result.Message = e.getMessage();
        }
        return result;
    }

    @ResponseBody
    @RequestMapping(value = "/targetPersons")
    public JTableDTO targetPersonsList(
        @RequestParam Integer jtStartIndex,
        @RequestParam Integer jtPageSize,
        @RequestParam(required = false) String jtSorting,
        @RequestParam(required = false) String name,
        @RequestParam(required = false) String twitterId
    ) {
        JTableDTO result = new JTableDTO();
        try {
            Sort sort = getSorting(jtSorting);
            PageRequest pageable = new PageRequest(jtStartIndex / jtPageSize, jtPageSize, sort);
            Page<TargetPersonInfoDTO> targetPeople = null;
            if (StringUtils.isBlank(name) && StringUtils.isBlank(twitterId)) {
                targetPeople = targetPersonService.findAll(pageable);
            } else if (!StringUtils.isBlank(twitterId)) {
                targetPeople = targetPersonService.findByTwitterIdLike(twitterId, pageable);
            } else if (!StringUtils.isBlank(name)) {
                targetPeople = targetPersonService.findByNameLike(name, pageable);
            }

            result.Records = targetPeople.getContent();
            result.TotalRecordCount = targetPeople.getTotalElements();
        } catch (Exception e) {
            result.Result = "ERROR";
            result.Message = e.getMessage();
            logger.error("Error on target person search", e);
        }
        return result;
    }

    @Override
    protected Sort getSorting(String jtSorting) {
        Sort sort = new Sort(Sort.Direction.ASC, "id");
        if (jtSorting != null && !jtSorting.isEmpty()) {
            Sort.Direction direction = Sort.Direction.ASC;
            if (jtSorting.contains("DESC")) {
                direction = Sort.Direction.DESC;
            }
            String sortField = jtSorting.substring(0, jtSorting.indexOf(" "));
            if (Rating.RATING_SORT_FIELDS.contains(sortField)) {
                sortField = TargetPersonRepository.RATING_SORT_FIELDS_PREFIX + sortField;
            }
            sort = new Sort(direction, sortField);
        }
        return sort;
    }

    @ResponseBody
    @RequestMapping(value = "/targetPersons/{id}/posts/fake")
    public JTableDTO getFakePostsOfATargetPersonList(@PathVariable Integer id) {
        JTableDTO result = new JTableDTO();
        result.Result = "OK";
        result.Records = targetPersonService.getFakePostsOfPersonWithId(id);
        result.TotalRecordCount = (long) result.Records.size();
        return result;
    }
}
