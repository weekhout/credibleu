package com.credibleu.controller;

import com.credibleu.domain.Account;
import com.credibleu.service.AccountService;
import com.credibleu.service.SecurityService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletResponse;
import java.util.Collections;
import java.util.Map;

/**
 * @author AGeranin (First Line Software)
 */
@Controller
public class AccountController {

    @Autowired
    private AccountService accountService;
    @Autowired
    private SecurityService securityService;

    /**
     * Adds new account to the platform.
     */
    @ResponseBody
    @RequestMapping(value = "/account", method = RequestMethod.POST)
    public Object addAccount(Account account, HttpServletResponse response) {
        Map<String, String> errors = accountService.validate(account);
        if (!errors.isEmpty()) {
            response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
            return Collections.singletonMap("errors", errors.values());
        }

        String password = account.getPassword();
        accountService.add(account);
        securityService.login(account.getUsername(), password);
        return null;
    }
}
