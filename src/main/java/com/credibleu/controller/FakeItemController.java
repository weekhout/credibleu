package com.credibleu.controller;

import com.credibleu.domain.FakeItem;
import com.credibleu.domain.FakeItemsSource;
import com.credibleu.domain.UncredibleSite;
import com.credibleu.dto.JTableDTO;
import com.credibleu.dto.JTableRecordDTO;
import com.credibleu.service.fakeitems.FakeItemsService;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;

/**
 * @author AGeranin (First Line Software)
 */
@Controller
public class FakeItemController extends JTableController {

    @Autowired
    private FakeItemsService fakeItemsService;

    @ResponseBody
    @RequestMapping(value = "/fakeItem", method = RequestMethod.POST)
    public JTableRecordDTO createFakeItem(@RequestParam String url) {
        JTableRecordDTO result = new JTableRecordDTO();
        try {
            UncredibleSite site = new UncredibleSite(url);
            FakeItem newFakeItem = fakeItemsService.create(site);
            result.Result = "OK";
            result.Record = newFakeItem;
        } catch (Exception e) {
            result.Result = "ERROR";
            result.Message = e.getMessage();
        }
        return result;
    }

    @ResponseBody
    @RequestMapping(value = "/fakeItems")
    public JTableDTO fakeItemsList(
        @RequestParam Integer jtStartIndex,
        @RequestParam Integer jtPageSize,
        @RequestParam(required = false) String jtSorting,
        @RequestParam(required = false) String url
    ) {
        JTableDTO result = new JTableDTO();
        try {
            Sort sort = getSorting(jtSorting);
            PageRequest pageable = new PageRequest(jtStartIndex / jtPageSize, jtPageSize, sort);
            Page<FakeItem> fakeItems;
            if (StringUtils.isBlank(url)) {
                fakeItems = fakeItemsService.findAll(pageable);
            } else {
                fakeItems = fakeItemsService.findByUrlLike(url, pageable);
            }
            result.Records = fakeItems.getContent();
            result.TotalRecordCount = fakeItems.getTotalElements();
        } catch (Exception e) {
            result.Result = "ERROR";
            result.Message = e.getMessage();

        }
        return result;
    }

    @ResponseBody
    @RequestMapping(value = "/fakeItems", method = RequestMethod.POST, consumes = {"multipart/form-data"})
    public void uploadFakeItems(@RequestPart("file") MultipartFile fakeNewsJson, FakeItemsSource source) throws IOException {
        fakeItemsService.parseAndAdd(fakeNewsJson.getInputStream(), source);
    }
}
