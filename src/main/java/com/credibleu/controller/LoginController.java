package com.credibleu.controller;

import com.credibleu.service.SecurityService;
import com.credibleu.service.SessionStore;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.AuthenticationException;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Collections;

/**
 * @author AGeranin (First Line Software)
 */
@Controller
public class LoginController {

    @Autowired
    private SecurityService securityService;
    @Autowired
    private SessionStore sessionStore;

    @ResponseBody
    @RequestMapping(value = "/login", method = RequestMethod.POST)
    public Object login(String username, String password, HttpServletResponse response) {
        try {
            securityService.login(username, password);
            return null;
        } catch (AuthenticationException e) {
            response.setStatus(HttpServletResponse.SC_UNAUTHORIZED);
            return Collections.singletonMap("errors", Collections.singletonList("Incorrect username or password"));
        }
    }

    @ResponseBody
    @RequestMapping(value = "/logout", method = RequestMethod.GET)
    public void logout(HttpServletRequest request) {
        request.getSession().invalidate();
    }

    @ResponseBody
    @RequestMapping(value = "/username", method = RequestMethod.GET)
    public String getUsername() {
        return sessionStore.getAccountMock().getUsername();
    }
}
