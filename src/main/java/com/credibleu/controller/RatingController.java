package com.credibleu.controller;

import com.credibleu.service.RatingCalculator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * @author AGeranin (First Line Software)
 */
@Controller
public class RatingController {

    @Autowired
    private RatingCalculator ratingCalculator;

    /**
     * Utility method which initiates rating recalculation for all target persons.
     * Just in case of something went wrong or some changes were made manually in the DB.
     */
    @ResponseBody
    @RequestMapping(value = "/recalculateRatings", method = RequestMethod.GET)
    public void recalculateRatings() {
        ratingCalculator.recalculateAllRatings();
    }
}
