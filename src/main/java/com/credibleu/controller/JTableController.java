package com.credibleu.controller;

import org.springframework.data.domain.Sort;

/**
 * @author AGeranin (First Line Software)
 */
public abstract class JTableController {

    protected Sort getSorting(String jtSorting) {
        Sort sort = new Sort(Sort.Direction.ASC, "id");
        if (jtSorting != null && !jtSorting.isEmpty()) {
            Sort.Direction direction = Sort.Direction.ASC;
            if (jtSorting.contains("DESC")) {
                direction = Sort.Direction.DESC;
            }
            String sortField = jtSorting.substring(0, jtSorting.indexOf(" "));
            sort = new Sort(direction, sortField);
        }
        return sort;
    }

}
