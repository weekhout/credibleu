# CredibleU
Since the US elections, fake news has become a major concern for those working in policy-making. Societal tensions have 
emerged across the country, partly because media reports are injected with false information and purposefully skewed 
headlines. With the French and German elections later this year, and the mid-term elections in the US in 2018, there is 
ample reason to develop strategies that can help avoid false information to spread.
 
With the CredibleU project, HumanityX has this prototype that targets politicians and their twitter behaviour.

# Requirements
CredibleU is a web application developed using Spring Boot framework.
Working application doesn't require a lot of resources.
* 1 CPU core with 1 GB RAM is enough to start, but 2 cores and 4 GB RAM is recommended.
* Application needs PostgreSQL (ver 9+) database to store data. Database URL and credentials could be set up in application.properties.
* To run application JDK version 1.8 or higher should be installed, JAVA_HOME environment variable set and JAVA_HOME/bin added to PATH.

# Running
Build application by running 'gradlew.bat clean build' ('gradlew clean build' for Linux) from the project root directory.
Then go to [project root dir]/build/distributions and unpack ZIP or TAR archive with application standalone version.
Go to result folder and then to bin folder. Run credibleu.bat (credibleu for Linux) to start the application.
Open browser and go to http://localhost:8080/credibleu to access application.

During start process application tries to get all tweets of all target persons from the database (skipping already existing).
This may take pretty much time. If you don't need this and just want to quickly run the application, you can disable this
by setting parameter 'twitter.adapter.get.history.on.start' to false in application.properties file.
If you also don't want to receive any tweets you can disable Twitter adapter by setting 'twitter.adapter.disabled' to true.